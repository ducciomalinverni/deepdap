#!/usr/bin/env python3

import argparse
import tensorflow as tf
import tensorflow_addons as tfa                 

def getParams():
    parser=argparse.ArgumentParser()

    parser.add_argument("--output","-o",default="output",type=str, help="Prefix of output files. Default=output")
    parser.add_argument("--trainSet","-t",type=str, help="File containing training set (tfrecord format)")
    parser.add_argument("--validSet","-v",type=str, help="File containing validation set (tfrecord format")
    parser.add_argument("--batch","-b",default=1,type=int, help="Mini-batch size. Default=1")
    parser.add_argument("--dropout","-d",default=0.15,type=float,help="Dropout factor. Default=0.15")
    parser.add_argument("--kernelSize","-k",default=3,type=int, help="Kernel size (isotropic). Default=3")
    parser.add_argument("--filters","-f",default=64,type=int,help="Number of filters per layer. Default=64")
    parser.add_argument("--layers","-l",default=5,type=int, help="Number of ResNet blocks. Default=5")
    parser.add_argument("--epochs","-e",default=10,type=int, help="Number of training epochs. Default=10")
    parser.add_argument("--stepsPerEpoch","-ts",default=100,type=int,
                        help="Number of steps per training epoch . Default=100")
    parser.add_argument("--validSteps","-vs",default=100,type=int,
                        help="Number of samples used to compute the validation metrics. Default=10")
    parser.add_argument("--gpuId","-g",default=-1,type=int,
                        help="The id of the GPU to use, if available. Default, all")
    parser.add_argument("--profile",default=False,help="Enable the Tensorboard profile logging",action="store_true")
    
    return parser.parse_args() 

def getTFdataset(dsFile,batchSize,cropSize=64,subB=100,numClasses=21,shuffle=128,repeat=None):
    #### Local map functions
    features_description = {
        'msa': tf.io.FixedLenSequenceFeature([],tf.int64,allow_missing=True),
        'shape': tf.io.FixedLenFeature([2],tf.int64),
    }

    def _parse_function(example_proto):
        return tf.io.parse_single_example(example_proto, features_description)

    def _reshape(input):
        return tf.reshape(input['msa'],input['shape'])

    def getCoFreqDS(input):
        cropSize=tf.cast(tf.shape(input)[1]/2,tf.int64)
        oh=tf.one_hot(input,depth=numClasses,dtype=tf.float32)
        cf = tf.tensordot(oh[:,:cropSize,:],tf.transpose(oh[:,cropSize:,:],[1,0,2]),axes=[0,1])
        cf = tf.transpose(cf,[0,2,1,3])
        sh=tf.shape(cf)
        cf = tf.reshape(cf,[sh[0],sh[1],sh[2]*sh[3]])/tf.cast(tf.shape(input)[0],tf.float32)
        return cf

    def getFreqDS(input):
        oh=tf.one_hot(input,depth=numClasses,dtype=tf.float32)
        return tf.stack([tf.reduce_mean(oh,axis=0)]*cropSize)
    
    def makeSample(input):
        subIdx=tf.random.shuffle(tf.range(0,tf.shape(input)[0],dtype=tf.int32))[:subB]
        subSampled= tf.gather(input,subIdx,axis=0)       
        return {'CoFreqsSub':getCoFreqDS(subSampled)},{'CoFreqs':getCoFreqDS(input),'Freqs':getFreqDS(input)}

    # Build the dataset as tf.data pipeline
    ds = tf.data.TFRecordDataset(dsFile)
    ds=ds.map(_parse_function,num_parallel_calls=tf.data.experimental.AUTOTUNE)
    ds=ds.map(_reshape,num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset=ds.map(makeSample).shuffle(shuffle).batch(batchSize).repeat(repeat).prefetch(tf.data.experimental.AUTOTUNE)   
    return dataset

class Marginalization(tf.keras.layers.Layer):
    def __init__(self,numClasses,name='Marginalization',**kwargs):
        super(Marginalization,self).__init__(name=name,**kwargs)
        self.numClasses=numClasses
    
    def get_config(self):
        config = super(Marginalization, self).get_config()
        config.update({'numClasses': self.numClasses})
        return config

    def call(self,inputs):
        Pij=tf.reshape(inputs,[-1,tf.shape(inputs)[1],tf.shape(inputs)[2],self.numClasses,self.numClasses])
        Pi=tf.reduce_sum(Pij,axis=4)
        Pj=tf.reduce_sum(Pij,axis=3)
        return tf.concat([tf.transpose(Pi,perm=[0,2,1,3]),Pj],axis=2)

class L1Normalization(tf.keras.layers.Layer):
    def __init__(self,name='L1Normalization',**kwargs):
        super(L1Normalization,self).__init__(name=name,**kwargs)

                           
    def get_config(self):
        config = super(L1Normalization, self).get_config()
        return config

    def call(self,inputs):
        return tf.linalg.normalize(inputs,axis=-1,ord=1)[0]
    
def buildModel(nResidualBlocks,nFilters,kernelSize,dropout,cropSize=64,numClasses=21):

    inputs = tf.keras.layers.Input(shape=(cropSize,cropSize,numClasses**2),name='CoFreqsSub')

    # Iteratively build stacked residual dilated-convolutional blocks 
    dilation=1
    net0 = tf.keras.layers.Conv2D(filters=nFilters,kernel_size=1,padding='same')(inputs)
    for i in range(nResidualBlocks):
        net= tf.keras.layers.Activation('elu')(net0)
        net = tf.keras.layers.Conv2D(filters=nFilters,kernel_size=kernelSize,
                                     dilation_rate=dilation,padding='same')(net)
        net = tfa.layers.InstanceNormalization(axis=3,center=True,scale=True)(net)
        net = net = tf.keras.layers.Activation('elu')(net)
        net = tf.keras.layers.Dropout(dropout)(net)
        net = tf.keras.layers.Conv2D(filters=nFilters,kernel_size=kernelSize,
                                     dilation_rate=dilation,padding='same')(net)
        net = tfa.layers.InstanceNormalization(axis=3,center=True,scale=True)(net)
        net = tf.keras.layers.add([net,net0])
        net0 = net
        
        dilation*=2
        if dilation==32:
          dilation=1
    net = tf.keras.layers.Conv2D(filters=numClasses**2,kernel_size=1,padding='same')(net)
    net= tf.keras.layers.Softmax(axis=-1)(net)
    
    # Add long range skip connection, allows (additively) modulating the co-frequencies
    net = tf.keras.layers.add([net,inputs])
    
    # Normalize co-frequencies to be blockwise normalized to 1 and marginalize each block along i and j
    coFreqs= L1Normalization(name='CoFreqs')(net)
    freqs = Marginalization(numClasses,name='Freqs')(coFreqs)
    model = tf.keras.Model(inputs=[inputs],outputs=[coFreqs,freqs])
    
    return model
                             
if __name__=="__main__":

    print("Setting up parameters")
    args = getParams()
    
    # If available, set the run on a predefined GPU
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if args.gpuId !=-1 and gpus:
        try:
            tf.config.experimental.set_visible_devices(gpus[args.gpuId], 'GPU')
        except RuntimeError as e:
            # Visible devices must be set at program startup
            print(e)

    
    print("Loading datasets")
    trainDS=getTFdataset(args.trainSet,args.batch)
    validDS=getTFdataset(args.validSet,args.batch)

    print("Building the model")
    model = buildModel(nResidualBlocks=args.layers,
                       nFilters=args.filters,
                       kernelSize=args.kernelSize,
                       dropout=args.dropout)

    model.compile(optimizer='adam',loss='MSE',loss_weights={'CoFreqs':1.,'Freqs':1.})
    
    callbacks=[tf.keras.callbacks.CSVLogger(args.output+"_log.tsv", append=False,separator='\t'),
               tf.keras.callbacks.ModelCheckpoint(args.output+"_cpt",save_freq='epoch')
               ]
    if args.profile:
        callbacks.append(tf.keras.callbacks.TensorBoard(log_dir = args.output+"_TB",
                                                        histogram_freq = 1,
                                                        profile_batch = "2,50"))
    print("Training the model")
    history=model.fit(x=trainDS,
                      validation_data=validDS,
                      epochs=args.epochs,
                      steps_per_epoch=args.stepsPerEpoch,
                      validation_steps=args.validSteps,
                      callbacks=callbacks)
    
    print("Saving final model")
    model.save(args.output+"_FinalModel")
