# deepDAP (deep Data Augmentation of Protein sequences)
Effective data augmentation by denoising of inter-residue correlation matrix.

**deepDAP** aims at improving the estimation of inter-residue correlation matrix by improving its estimation in low sampling regimes.
It is built by training a deep model to estimate a inter-residue correlation matrix from a noisy version obtained by sub-sampling the available sequences.
The subsampling is done as a preprocessing step using pipelining available through tf.data.

The model architectures consists of M residual blocks, each composed of an ELU activation, a convolutional layer, a InstanceNorm layer, a dropout layer, a convolutional layer and another InstanceNorm layer. Skip connections are present between each consecutive block.
Convolutions are cyclically dilated by a factor of 2 at each blcok. A long skip connection between input and output is added to help propagate the initial data deeper into the model. The model has custom marginalization and L1-normalization layers that computes P_i(A) and P_j(B) from P_ij(A,B) and correclty normalize the probabilities, which enforce the consistency of the denoised correlation blocks.
The total loss is composed of a loss on the denoised correlations P_ij(A,B) and on the marginals P_i(A) and P_j(B). 

deepDAP is trained on 64x64x441 blocks of the inter-residue covariance matrix computed over  a clustered version of the PFAM32 release. To avoid data leakage during training, the PFAM families are grouped by PFAM Clans, and the train/validation splis is done at the clan level.



# Requirements
  * tensorflow >=2.3.1
  * tensorflow-addons >=0.12.0
  * argparse >=1.1
  * numpy >=1.18.5
  * [dcaTools](https://gitlab.com/ducciomalinverni/dcaTools)
  * [hhsuite](https://github.com/soedinglab/hh-suite) >= 3.3 (used for dataset preparation)

# Usage
To train deepDAP with default parameters call

```shell
$ ./trainModel.py -t train.tfrecord -v valid.tfrecord -o outPrefix
```

To see all available options

```shell
$ ./trainModel.py -h

usage: trainModel.py [-h] [--output OUTPUT] [--trainSet TRAINSET] [--validSet VALIDSET] [--batch BATCH] [--dropout DROPOUT] [--kernelSize KERNELSIZE] [--filters FILTERS]
                     [--layers LAYERS] [--epochs EPOCHS] [--stepsPerEpoch STEPSPEREPOCH] [--validSteps VALIDSTEPS] [--gpuId GPUID] [--profile]

optional arguments:
  -h, --help            show this help message and exit
  --output OUTPUT, -o OUTPUT
                        Prefix of output files. Default=output
  --trainSet TRAINSET, -t TRAINSET
                        File containing training set (tfrecord format)
  --validSet VALIDSET, -v VALIDSET
                        File containing validation set (tfrecord format
  --batch BATCH, -b BATCH
                        Mini-batch size. Default=1
  --dropout DROPOUT, -d DROPOUT
                        Dropout factor. Default=0.15
  --kernelSize KERNELSIZE, -k KERNELSIZE
                        Kernel size (isotropic). Default=3
  --filters FILTERS, -f FILTERS
                        Number of filters per layer. Default=64
  --layers LAYERS, -l LAYERS
                        Number of ResNet blocks. Default=5
  --epochs EPOCHS, -e EPOCHS
                        Number of training epochs. Default=10
  --stepsPerEpoch STEPSPEREPOCH, -ts STEPSPEREPOCH
                        Number of steps per training epoch . Default=100
  --validSteps VALIDSTEPS, -vs VALIDSTEPS
                        Number of samples used to compute the validation metrics. Default=10
  --gpuId GPUID, -g GPUID
                        The id of the GPU to use, if available. Default, all
  --profile             Enable the Tensorboard profile logging

```

# Building the training set
Scripts to preprocess the PFAM dataset to build the training, validation and test sets are located in the datasetPreparation folder.
* After downloading the full PFAM release as a stockholm file, use splitPFAMtoFastas.py to get individual families MSAs.
* FilterMSAs is used to remove overly gapped sequences and reduce redundancy by similarity filtering.
* SelectFamilies splits the training, validation and test sets taking in account the Clan structure of related families to avoid data leakage.
* PrepareSets.py computes the inter-residue NxNx441 inter-residue correlation matrix for each family, pads them to have shape multiple of 64x64x441, splits them into 64x64x441 blocks and saves the blocks as sets in tfrecord format.