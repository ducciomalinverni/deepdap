#!/usr/bin/env python3

import tensorflow as tf
import sequenceHandler as sh
import numpy as np

import trainModel

def testCoFreqs(msaPath,listFile,datasetFile,cropSize=64,errorTol=1e-8,verbose=True):
    """ Compare manually computed co-frequencies with co-frequencies computed in the tf.data pipeline.
    """

    # Load the dataset
    dataset=trainModel.getTFdataset(datasetFile,batchSize=1,shuffle=1,repeat=1)
    it=iter(dataset)

    print("*** Testing Co-frequency calculation ......\t",end="",flush=True)
    with open(listFile,'r') as f:
        for l in f:
            # Sequentially load the MSAs and pad 
            msa,_=sh.fastaToMatrix(msaPath+l.rstrip()+'_90wid.fasta')
            if msa.shape[1]%cropSize !=0:
                msa=np.hstack((msa,
                               np.zeros((msa.shape[0],int((1+np.floor(msa.shape[1]/cropSize))*cropSize-msa.shape[1])))))
            N=msa.shape[1]
            
            # Compute co-frequencies manually
            origMSA=sh.binarizeMSA(msa)
            fij=(origMSA.T.dot(origMSA)/float(origMSA.shape[0]))
            fijL=np.zeros((N,N,21*21))
            for i in range(N):
                for j in range(N):
                    fijL[i,j,:]=fij[21*i:21*(i+1),21*j:21*(j+1)].ravel()

            # For each crop, compare computed co-freqs to co-freqs computed with the tf.data pipeline 
            for i in range(int(N/cropSize)):
                for j in range(i,int(N/cropSize)):                    
                    fijNp=fijL[cropSize*i:cropSize*(i+1),cropSize*j:cropSize*(j+1),:]
                    fijTf=next(it)[1]['CoFreqs'].numpy()
                    error=((fijNp.ravel()-fijTf.ravel())**2).sum()
                    if verbose:
                        print("i=",i,", j=",j,", fijNP.shape=",fijNp.shape,", fijTF.shape=",fijTf.shape,", Error=",error)
                    assert error<errorTol
    print("passed.")

def testFreqs(msaPath,listFile,datasetFile,cropSize=64,numClasses=21,errorTol=1e-8,verbose=True):
    """ Compare manually computed frequencies with frequencies computed in the tf.data pipeline.
    """
    # Load the dataset
    dataset=trainModel.getTFdataset(datasetFile,batchSize=1,shuffle=1,repeat=1)
    it=iter(dataset)

    print("*** Testing single-site frequency calculation......\t",end="",flush=True)
    with open(listFile,'r') as f:
        for l in f:
            # Sequentially load the MSAs and pad 
            msa,_=sh.fastaToMatrix(msaPath+l.rstrip()+'_90wid.fasta')
            if msa.shape[1]%cropSize !=0:
                msa=np.hstack((msa,
                               np.zeros((msa.shape[0],int((1+np.floor(msa.shape[1]/cropSize))*cropSize-msa.shape[1])))))
            N=msa.shape[1]

            # Compute frequencies manually
            origMSA=sh.binarizeMSA(msa)
            fi=origMSA.mean(axis=0)
            fiL=np.zeros((N,21))
            for i in range(N):
                fiL[i,:]=fi[21*i:21*(i+1)]
            
            for i in range(int(N/cropSize)):
                for j in range(i,int(N/cropSize)):
                    fiNp=fiL[cropSize*i:cropSize*(i+1),:]
                    fjNp=fiL[cropSize*j:cropSize*(j+1),:]
                    fijNp=np.vstack((fiNp,fjNp))
                    fijTf=next(it)[1]['Freqs'].numpy()
                    for nRep in range(fijTf.shape[1]):
                        error=((fijNp.ravel()-fijTf[0,nRep,:,:].ravel())**2).sum()
                        if verbose:
                            print("i=",i," j=",j," nRep=",nRep," fijNP.shape=",fijNp.shape,
                                  " fijTF.shape=",fijTf[0,nRep,:,:].shape," Error=",error)
                        assert error<errorTol
    print("passed.")

def testMarginalization(msaPath,listFile,datasetFile,cropSize=64,numClasses=21,verbose=True,errorTol=1e-8):
    """ Compare manually computed frequencies with frequencies computed by the 
        marginalization layer applied to the tf.data co-frequencies.
    """
    # Load the dataset
    dataset=trainModel.getTFdataset(datasetFile,batchSize=1,shuffle=1,repeat=1)
    it=iter(dataset)
    
    # Build a simple model which only computes the marginalization from the co-freqs in the dataset
    inputs = tf.keras.layers.Input(shape=(cropSize,cropSize,numClasses**2))
    freqs = trainModel.Marginalization(numClasses)(inputs)
    model = tf.keras.Model(inputs=inputs,outputs=freqs)

    print("*** Testing consistency of Marginalization layer......\t",end="",flush=True)    
    with open(listFile,'r') as f:
        for l in f:
            msa,_=sh.fastaToMatrix(msaPath+l.rstrip()+'_90wid.fasta')
            if msa.shape[1]%cropSize !=0:
                
                msa=np.hstack((msa,
                               np.zeros((msa.shape[0],int((1+np.floor(msa.shape[1]/cropSize))*cropSize-msa.shape[1])))))
            N=msa.shape[1]
            origMSA=sh.binarizeMSA(msa)
            fi=origMSA.mean(axis=0)
            fiL=np.zeros((N,21))
            for i in range(N):
                fiL[i,:]=fi[21*i:21*(i+1)]
            
            for i in range(int(N/cropSize)):
                for j in range(i,int(N/cropSize)):
                    fiNp=fiL[cropSize*i:cropSize*(i+1),:]
                    fjNp=fiL[cropSize*j:cropSize*(j+1),:]
                    fNp=np.vstack((fiNp,fjNp))
                    fTf=model(next(it)[1]['CoFreqs']).numpy()[0,1,:,:]
                    error=((fNp.ravel()-fTf.ravel())**2).sum()
                    if verbose:
                        print("i=",i," j=",j," fNp.shape=",fNp.shape," fTf=",fTf.shape," Error=",erorr)
                    assert error<errorTol
                    
    print("passed.")
    
if __name__=="__main__":
    testCoFreqs('data/sequences/90wid_75cov/',
                'sets/miniSets/mini_train.dat',
                'sets/miniSets/mini_train.tfrecord',verbose=False)

    testFreqs('data/sequences/90wid_75cov/',
              'sets/miniSets/mini_train.dat',
              'sets/miniSets/mini_train.tfrecord',verbose=False)

    testMarginalization('data/sequences/90wid_75cov/',
                        'sets/miniSets/mini_train.dat',
                        'sets/miniSets/mini_train.tfrecord',verbose=False)
