#!/bin/bash

tmp=tmp6

while read f
do
    #echo $f
    filterMSAByGapContent $f 0.25 $tmp > /dev/null
    hhfilter -i $tmp -id 90 -o data/sequences/90wid_75cov/${f:52:7}_90wid.fasta 2>/dev/null
    #rm $tmp
done <$1
