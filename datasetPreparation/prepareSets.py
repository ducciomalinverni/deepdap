#!/usr/bin/env python3

import numpy as np
import sequenceHandler as sh
import tensorflow as tf
import sys

def buildTFset(path,fileList,outFile):
    
    with open(fileList,'r') as f:
        writer = tf.io.TFRecordWriter(outFile)
        for i,l in enumerate(f):
            msa,_ = sh.fastaToMatrix(path+l.rstrip()+'_90wid.fasta')
            feature = {'msa': tf.train.Feature(int64_list=tf.train.Int64List(value=msa.flatten())),
                       'shape': tf.train.Feature(int64_list=tf.train.Int64List(value=msa.shape))
            }
            example = tf.train.Example(features=tf.train.Features(feature=feature))
            writer.write(example.SerializeToString())
        writer.close()

def buildNPset(path,fileList,outFile):
    msas=[]
    
    with open(fileList,'r') as f:
        for i,l in enumerate(f):
            msa,_=sh.fastaToMatrix(path+l.rstrip()+'_90wid.fasta')
            msas.append(msa.astype(np.uint8))
            if not i%100:
                print(i)
                
    np.savez(outFile,*msas)
    
def buildTFset2(path,fileList,outFile,cropSize=64):
    #Split each MSA into multiple paired sub-MSAs of size Bx(2xcropSize)
    #Pad the end with 0
    with open(fileList,'r') as f:
        writer = tf.io.TFRecordWriter(outFile)
        for i,l in enumerate(f):
            msa,_ = sh.fastaToMatrix(path+l.rstrip()+'_90wid.fasta')
            # Pad with gaps
            if msa.shape[1]%cropSize!=0:
                paddings=np.asarray([[0,0],[0,(1+np.floor(msa.shape[1]/cropSize))*cropSize-msa.shape[1]]],dtype=int)
                msa=np.pad(msa,paddings,mode='constant',constant_values=0)
            print(l.rstrip(),msa.shape)
            # Split into blocks of cropSize and concatenate 
            for i in range(int(np.ceil(msa.shape[1]/cropSize))):
                for j in range(i,int(np.ceil(msa.shape[1]/cropSize))):
                    print(i,j)
                    ms=np.hstack((msa[:,cropSize*i:cropSize*(i+1)],msa[:,cropSize*j:cropSize*(j+1)]))
                    feature = {'msa': tf.train.Feature(int64_list=tf.train.Int64List(value=ms.flatten())),
                               'shape': tf.train.Feature(int64_list=tf.train.Int64List(value=ms.shape))
                    }
                    example = tf.train.Example(features=tf.train.Features(feature=feature))
                    writer.write(example.SerializeToString())
            if not i%100:
                print(i)
        writer.close()
        
def getTFdataset(dsFile):
    features_description = {
        'msa': tf.io.FixedLenSequenceFeature([],tf.int64,allow_missing=True),
        'shape': tf.io.FixedLenFeature([2],tf.int64),
    }
    def _parse_function(example_proto):
        return tf.io.parse_single_example(example_proto, features_description)
    def _reshape(input):
        return tf.reshape(input['msa'],input['shape'])
    
    ds = tf.data.TFRecordDataset(dsFile)
    ds=ds.map(_parse_function).map(_reshape)
    for x in ds:
        print(x)
        
if __name__=="__main__":

    path='data/sequences/90wid_75cov/'

    print("TrainingSet")
    buildTFset2(path,'sets/trainSet.dat','sets/trainSet.tfrecord')
    print("ValidSet")
    buildTFset2(path,'sets/validSet.dat','sets/validSet.tfrecord')
    print("TestSet")
    buildTFset2(path,'sets/testSet.dat','sets/testSet.tfrecord')

    #buildTFset(path,'sets/mini_train.dat','mini_train.tfrecord')
    #buildTFset(path,'sets/mini_valid.dat','mini_valid.tfrecord')
