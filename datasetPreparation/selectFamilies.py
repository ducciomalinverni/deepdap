#!/usr/bin/env python3
import sys
import glob
import Bio.SeqIO
import numpy as np
import matplotlib.pyplot as plt

if __name__=="__main__":

    minB=500
    minN=64
    
    baseDir="data/"
    
    msaFolder=baseDir+'sequences/90wid_75cov/'
    pfStats=baseDir+'sequences/90wid_75cov/pfamStats_90wid.dat'
    pfClans=np.loadtxt(baseDir+'pfam/PFAM32/Pfam-A.clans.tsv',dtype=str,delimiter='\t')
    
    # Get or Build Pfam B and N statistics
    if not glob.glob(pfStats):
        nFams=len(glob.glob(msaFolder+'*_90wid.fasta'))
        pfamStats=np.zeros((nFams,2),dtype=int)
        pfs=[]
        for i,f in enumerate(glob.glob(msaFolder+'*_90wid.fasta')):
            pfamStats[i,0]=open(f,'r').read().count(">")
            pfamStats[i,1]=len(next(Bio.SeqIO.parse(f,'fasta')).seq)
            pfs.append(f.split('/')[3].split('_')[0])
            print(i)
        pfs= np.asarray(pfs,dtype=str)
        np.savetxt(msaFolder+'pfamStats_90wid.dat',
                   np.hstack((pfs.reshape(nFams,1),pfamStats.astype(str))),fmt='%s')
    else:
        pfamStats=np.loadtxt(pfStats,dtype=str)
        pfs=pfamStats[:,0]
        pfamStats=pfamStats[:,1:].astype(float)
    
    print("Number of families with B>",minB," and N>",minN,": ",((pfamStats[:,0]>minB)*(pfamStats[:,1]>minN)).sum())

    selectedPfs=pfs[(pfamStats[:,0]>minB)*(pfamStats[:,1]>minN)]
    open('pfsSelected.dat','w').write("\n".join(selectedPfs))

    # Split families into train,validation and test set
    # Ensuring that no families belonging to the same clan are present in different sets
    # (Avoid data-leakeage)

    fractions=[0.9,0.05,0.05]
    for i in range(pfClans.shape[0]):
        if pfClans[i,1]=='':
            pfClans[i,1]=str(i)

    clans=np.unique(pfClans[:,1])
    np.random.shuffle(clans)

    trainSet=[]
    validSet=[]
    testSet=[]
    for clan in clans:
        if len(trainSet)<(fractions[0]*selectedPfs.shape[0]):
            for pf in pfClans[pfClans[:,1]==clan,0]:
                if pf in selectedPfs:
                    trainSet.append(pf)
        elif len(validSet)<(fractions[1]*selectedPfs.shape[0]):
            for pf in pfClans[pfClans[:,1]==clan,0]:
                if pf in selectedPfs:
                    validSet.append(pf)
        else:
            for pf in pfClans[pfClans[:,1]==clan,0]:
                if pf in selectedPfs:
                    testSet.append(pf)

    open('trainSet.dat','w').write("\n".join(trainSet))
    open('validationSet.dat','w').write("\n".join(validSet))
    open('testSet.dat','w').write("\n".join(testSet))

    print("Train set size:",len(trainSet),"(",100*len(trainSet)/float(len(selectedPfs)),"%)")
    print("Validation set size:",len(validSet),"(",100*len(validSet)/float(len(selectedPfs)),"%)")
    print("Test set size:",len(testSet),"(",100*len(testSet)/float(len(selectedPfs)),"%)")
    print("Total set size:",len(trainSet)+len(validSet)+len(testSet))
    
